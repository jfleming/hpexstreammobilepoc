            var cb;
            var reqStatus;
            var reqFlag;
            
            // If you want to prevent dragging, uncomment this section
            /*
             function preventBehavior(e) { 
             e.preventDefault(); 
             };
             document.addEventListener("touchmove", preventBehavior, false);
             */
            
            function onBodyLoad() {
                document.addEventListener("deviceready",onDeviceReady,false);
                
                if (navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/iPad/i)) {
                    $(document).ready(function () {
                                      $('label[for]').click(function () {
                                                            var el = $(this).attr('for');
                                                            if ($('#' + el + '[type=radio], #' + el + '[type=checkbox]').attr('selected', !$('#' + el).attr('selected'))) {
                                                            return;
                                                            } else {
                                                            $('#' + el)[0].focus();
                                                            }
                                                            });
                                      });
                }
                
                
            }
            
            /* When this function is called, PhoneGap has been initialized and is ready to roll */
            function onDeviceReady() {
                // IMPORTANT: must start notify after device is ready,
                // otherwise you will not be able to receive the launching notification in callback
                
                window.plugins.pushNotification.log("onDeviceReady called");
                window.plugins.pushNotification.startNotify();
                
                //Install child browser
                cb = ChildBrowser.install();
                if(cb != null)
                {
                    cb.onLocationChange = function(loc){ root.locChanged(loc); };
                    cb.onClose = function(){onCloseBrowser();};
                    cb.onOpenExternal = function(){root.onOpenExternal();};
                }
            }
        
            
            function onCloseBrowser()
            {
				// Do we need this?
				/*var pageID = $.mobile.activePage.attr('id');
                
				window.location.href("#eobsummary");*/
                
            }
                        
            
            // Customized callback for receiving notification
            PushNotification.prototype.notificationCallback = function (notification) {
                
                window.plugins.pushNotification.log("Received a notification.");
                
                var filename = notification['test'];
                var alertMessage = notification['alert'];
                
                getPage('#eobsummary div.PCPInfoBG', filename);
                
                if (confirm(alertMessage + '\nWould you like to view it now?'))
                {
                    $.mobile.changePage('#eobsummary');
                }
            };
            
            
            // when APN register succeeded
            function successCallback(e) {
                registerUAPush(e.deviceToken, e.host, e.appKey, e.appSecret);
            }
            
            // when APN register failed
            function errorCallback(e) {
                alert('Error during registration: ' + e.error);
                //registerButton.disabled=false;
            }
            
            // when APN register succeeded
            /*function successCallbackUnregister(e) {
                unregisterUAPush(e.deviceToken, e.host, e.appKey, e.appSecret);
            }*/
            
            // registering to APN 
            function registerAPN(isRegister) {
                
                window.plugins.pushNotification.log("Registering with APNS via the App Delegate");
                                
                if (isRegister)
                {
                    window.plugins.pushNotification.register(successCallback, errorCallback, [{ alert:true, badge:true, sound:true }]);
                }
                /*else
                {
                    window.plugins.pushNotification.register(successCallbackUnregister, errorCallback, [{ alert:true, badge:true, sound:true }]);               
                }*/
                
            }
            
            // register urban airship push service after APN is registered successfully
            function registerUAPush(deviceToken, host, appKey, appSecret) {
                
                window.plugins.pushNotification.log("Registering with Urban Airship.");
                
                var request = new XMLHttpRequest();
                
                // open the client and encode our URL
                request.open('PUT', host+'api/device_tokens/'+deviceToken+'/', true, appKey, appSecret);
                
                request.setRequestHeader('Content-Type','application/json');
                
                var userName = localStorage.getItem('username');
                
                var payload = {
                    "alias": String(userName)
                }
                
                // callback when request finished
                request.onload = function() {
                    
                    if(this.status == 200 || this.status == 201) {
                        alert("EOB Summary notifications successfully registered.");                  
                        window.localStorage.setItem("register", "1");
                        window.localStorage.setItem("host", host);
                        window.localStorage.setItem("deviceToken", deviceToken);
                        window.localStorage.setItem("appKey", appKey);
                        window.localStorage.setItem("appSecret", appSecret);
                    } else {
                        alert("Error Code: " + this.status + "\nStatus Text: " + this.statusText);
                        
                        window.localStorage.setItem("register", "0");
                    }
                };
                
                request.withCredentials = "true";
                
                request.send(JSON.stringify(payload));
            }
            
 /*           function unregisterUAPush(deviceToken, host, appKey, appSecret) {
                
                window.plugins.pushNotification.log("Registering with Urban Airship.");
                
                var request = new XMLHttpRequest();
                
                alert(host+'api/device_tokens/'+deviceToken+'/');
                
                // open the client and encode our URL
                request.open('DELETE', host+'api/device_tokens/'+deviceToken+'/', true, appKey, appSecret);

                // callback when request finished
                request.onload = function() {
                    
                    if(this.status == 204) {
                        alert("EOB Summary notifications successfully unregistered.");                  
                        window.localStorage.setItem("register", "0");
                    } else {
                        alert("Error while unregistering: " + this.status + "\nStatus Text: " + this.statusText);
                        
                        window.localStorage.setItem("register", "1");
                    }
                };
                
                request.withCredentials = "true";
                
                request.send();
            }*/
			
		$('#login').live('pageshow', function(event) {
			$('#loginSubmit').submit(function() {
                                     
                                     //if (isRegistered())
                                     //alert('registered');
									 
									 var username = $('#usernameText').val();
																	  
									 if (username.length <= 0)
									 {
										alert('Please enter a valid username to login.');
										return false;
									 }
									 
									 localStorage.clear();
									 
									window.localStorage.setItem("username", username);
									 
									 var timestamp = new Date().getTime();
									 
									 window.localStorage.setItem("timestamp", timestamp);
									 
									 return true;
									 });
		});

        /*$('#idcard').live('pagebeforeshow', function(event) {
                          getPage('#idcard div.PCPInfoBG', 'idcard.html');    
                          
                          });    */
        
        $('#eobsummary').live('pagebeforeshow', function(event) {
                          getPage('#eobsummary div.PCPInfoBG', 'eobsummary.html');    
                          
                          });    
        
        $('#homescreen').live('pagebeforeshow', function(event) {
                              
                              var registered = window.localStorage.getItem("register");
                              
                              if (registered == "1")
                              {
	                              $('registeredCheckbox').prop("checked", true);
                              }
                              else
                              {
    	                          $('registeredCheckbox').prop("checked", false);
        	                      window.localStorage.setItem("register", "0");
                              }
                              
		});       
        
        function showEOBDetails() {
            var strPath = window.location.href; 
            var path = strPath.substr(0,strPath.lastIndexOf('/')) + '/EOBDetails.pdf'; 
            
            window.plugins.childBrowser.showWebPage(encodeURI(path), { showLocationBar: false });            
            
            /*window.plugins.childBrowser.showWebPage('http://hpexstream:password@demo.nttdataadm.com/projects/hp/hpexstream/EOBDetails.pdf', { showLocationBar: false });*/
        }; 

        function showIDCard() {
            var strPath = window.location.href; 
            var path = strPath.substr(0,strPath.lastIndexOf('/')) + '/idcardrotated.html'; 
            
            window.plugins.childBrowser.showWebPage(encodeURI(path), { showLocationBar: false });
        }; 
		                
        function unregisterAPN()
        {
            
            var host = window.localStorage.getItem("host");
            var deviceToken = window.localStorage.getItem("deviceToken");
            var appKey = window.localStorage.getItem("appKey");
            var appSecret = window.localStorage.getItem("appSecret");
            
            var request = new XMLHttpRequest();
            
            // open the client and encode our URL
            request.open('DELETE', host+'api/device_tokens/'+deviceToken+'/', true, appKey, appSecret);
            
            request.onload = function() {
                
                if(this.status == 204) {
                    alert("EOB Summary notifications successfully unregistered.");                  
                    window.localStorage.setItem("register", "0");
                } else {
                    alert("Error while unregistering: " + this.status + "\nStatus Text: " + this.statusText);
                    
                    window.localStorage.setItem("register", "1");
                }
            };
            
            request.send();            
        }

function isRegistered(){
    var host = window.localStorage.getItem("host");
    var deviceToken = window.localStorage.getItem("deviceToken");
    var appKey = window.localStorage.getItem("appKey");
    var appSecret = window.localStorage.getItem("appSecret");
        
    var request = new XMLHttpRequest();
    
    alert(appKey);
    alert(appSecret);
    
    // open the client and encode our URL
    request.open('GET', host+'api/device_tokens/', true, appKey, appSecret);

    request.onload = function() {
        
        alert(this.responseText);
        
        if(this.status == 200) {
            alert("Registrations pulled.");                  
            return true;
        } else {
            alert("Error while getting registrations: " + this.status + "\nStatus Text: " + this.statusText);
            return false;
        }
    };
    
    //request.withCredentials = "true";
    alert('sending');
    request.send();            
    
}
        
        function getPage(target, filename){
            var pageLoaded = window.localStorage.getItem(filename);
            if (pageLoaded != 1)
            {
                
                var strPath = window.location.href; 
                var path = strPath.substr(0,strPath.lastIndexOf('/')) + '/' + filename; 
                
                var request = new XMLHttpRequest();
                /*request.open('GET', "http://demo.nttdataadm.com/projects/hp/hpexstream/"+filename, true, "hpexstream", "password");*/
                request.open('GET', encodeURI(path), true, "", "");
                /*request.open('GET', encodeURI(path), true, "hpexstream", "password");*/
                request.onload = function() {
                    
                    $(target).append(this.responseText);
                    try
                    {
                        $(target).trigger('create');
                    }
                    catch(err)
                    {
                        try{ 
							$(target).parent().trigger('create'); }
                        catch(err){ 
							alert('Error pulling content: ' + err.message); }
                    }
                    window.localStorage.setItem(filename, 1);
                };
                request.send();
            }
        };
        
        function registerOnLoad() {
            
            var userName = window.localStorage.getItem("username");
            
            if (userName != "")
            {   
                if (confirm("Do you want to register for push notifications?"))
                {
                    registerAPN(true);
                    return true;
                } 
            }
            else 
            {
                alert("Username is required for registering");
            }
            return false;
        };
        
        
        function unregister() {

            if (confirm("Do you want to unregister for push notifications?") )
            {
                unregisterAPN();
                return true;
            } 
            return false;
        };    
        
        function processNotifications(isRegistering) {

            if (isRegistering)
            {
                return registerOnLoad();
            }
            else
            {
                return !unregister();
            }
        }

    /*$("#regCheck").bind("change", function(event, ui)
        {
                        alert('in here');
                                   var isRegistering = true;
                                   if ($(this).is(":checked"))
                                   {
                                        isRegistering = false;
                                   }
                                   alert(isRegistering);
                        });*/
        
        function validateSession() {
            
            var sessionTimeout = 600; // in seconds
            
            var validSession = true;
            
            var username = localStorage.getItem('username');
            
            var entryTime = localStorage.getItem('timestamp');
            
            var currentTime = new Date().getTime();
            
            var sessionLength = -1;
            
            if ((entryTime != null) && (currentTime != null))
            {
                sessionLength = currentTime - entryTime;
            }
            
            if ((username == null) || (username == 0) || (sessionLength < 0) || (sessionLength > sessionTimeout * 1000))
            {
                alert('Session has timed out, please login again');
                
                localStorage.clear();
                
                document.location = '#login';
            }
			else
			{
				localStorage.setItem('timestamp', currentTime);
			}	
        }
        
        $('[data-role]=page').live('pagebeforeshow', function(event, ui) {
                                   var pageID = $.mobile.activePage.attr('id');
                                   
                                   if (pageID != 'login')
                                   {
                                   		validateSession();
                                   }
                                   
                                   $("#regCheck").bind("click", function(event, ui)
                                                       {
                                                       event.stopImmediatePropagation();
                                                       
                                                       $(this).attr('checked', processNotifications($(this).is(":checked")));
                                                       
                                                       });
                                   
                                   });
        
